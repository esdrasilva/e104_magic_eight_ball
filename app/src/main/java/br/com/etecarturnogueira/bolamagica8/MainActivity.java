package br.com.etecarturnogueira.bolamagica8;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.imageViewBolaMagica);
    }

    public void perguntar(View view){
        Random random = new Random();
        int imagem = random.nextInt(8) + 1;

        String nomeRecurso = String.format("resposta_%s", imagem);

        int drawable = getResources().getIdentifier(nomeRecurso, "drawable",
                getPackageName());

        imageView.setImageResource(drawable);
    }

    public void reiniciar(View view){
        imageView.setImageResource(R.drawable.inicio);
    }
}
